This repository contains:

* An example file structure that can be used to manage large long term Arduino projects in the SketchBook folder
* An example file structure that can be used for managing associated Techical notes related to the project
* A self study set of tutorials is provided in Basic Arduino skills to guide the student in hooking up arduino hardware if they are not familiar with it.
* A copy of Guidelines for writing software, which is intended to guide students new to software development in a systems engineering context. 