this folder contains a location for your projects documentation 

With a systems engineering prospective, you can expect to find documents similar to those presented here. Typically you will find:

1) a work package discription/ work breakdown schedule, 
	- This or these documents will discuss how you plan on tackling the project and will discuss your milestones (both major and minor) 	- This usually is a living document (with some fixed releases) and may include something to track current progress. 



2) A Requirements / Prelimninary Design Document
	- This or these documents will discuss your requirements and an intial design that meets (or should meet) those requirements. 
	- Make sure your your requirements are trackable/testable i.e. they can be validated by design, inspection, analysis or direct testing.  
	- Your intial design doesn't have to be incredibly detailed but should include enough detail for you to identify interfaces between major functions and define tests that will ensure you meet the minimum performance goals  from your requirements



3) Test Plan 
	- This or these documents will record the test procedures used to validate your system against the requirements. it should cover all all your requirements and show the links between your tests and the requirements very important for any requirements you planned to validate via testing. 



4) Detailed Design
	- This or these documents will discuss the details of the design, and will go in to any sort of mathmatical detail or analysis that can be used to validate design choices and software design. in a way this document should help you fill in the details of all your major functions, 



5) Validation of the design 
	- This or these documents will demonstrate the results of  your test plan and hopefully will further demonstrate that you have met all requirements. If there are any missed requirements they will be discussed in detail along with any remedial action that can be taken in the future  to correct the discrepancies. 

What is currently found here is a template for a project document in word format and the word version of the guiedlines for writing software pdf which is found at the root folder. 

