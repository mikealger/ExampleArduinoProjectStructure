The Libraries found in this folder include:
* FUNCTION1: an example of a blank library using the older simplified Arduino library format 
* FUNCTION2: a more detailed example library refreshing concepts of passing variables using C
* FUNCTION3: a copy of function 2 with the updated Arduino library format 
* a copy of HMC5883L as a demonstration showing how to use git submodules to link to another’s work directly 
* a copy of Time as another demonstration using a lazy copy and paste (which may be better than nothing)
