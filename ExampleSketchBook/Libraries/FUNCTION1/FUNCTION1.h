/*******************************************************************************
 *
 *  Purpose: Header for FUNCTION1 component
 *
 *
 *  Date:
 *  Author: 
 *  
 *******************************************************************************/
 /******************************************************************************
 * Includes
 *******************************************************************************/

 /******************************************************************************
 * DEFINES
 *******************************************************************************/
// Define guards for this header 
#ifndef FUNCTION1_H
#define FUNCTION1_H

// Some parameter related to FUNCTION2
#define FUNCTION1_PARAMETER 10

// Define guards to make sure .c functions/objects play nicely with .cpp functions/objects
#ifdef __cplusplus
extern "C" {
#endif

 /******************************************************************************
 * Function Prototypes 
 *******************************************************************************/
void iniFUNCTION1();
void FUNCTION1();
void termFUNCTION1();

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // FUNCTION1_H