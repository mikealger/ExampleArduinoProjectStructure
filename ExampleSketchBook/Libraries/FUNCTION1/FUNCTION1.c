/*******************************************************************************
 * FUNCTION1
 *
 *  Purpose:  Source code that implements FUNCTION1 component
 *
 *
 *  Date:
 *  Author: 
 *  
 *******************************************************************************/
 /******************************************************************************
 * Includes
 *******************************************************************************/
#include <FUNCTION1.h>

 /******************************************************************************
  * iniFUNCTION1
  *
  *  purpose : initializes pins and memory used by FUNCTION1
  *
  *  inputs: N/A
  *  outputs: N/A
  *  effect: 
  ******************************************************************************/
void iniFUNCTION1() {
  // put your setup code here, to run once:

}

 /******************************************************************************
  * FUNCTION1
  *
  *  purpose : implements the main procedure of ..... 
  *
  *  inputs: N/A
  *  outputs: N/A
  *  effect: main routine of FUNCTION1. 
  ******************************************************************************/
void FUNCTION1() {
  // put your main code here, to run repeatedly:

}

 /******************************************************************************
  * iniFUNCTION1
  *
  *  purpose : terminates anything initialized for FUNCTION1
  *
  *  inputs: N/A
  *  outputs: N/A
  *  effect: 
  ******************************************************************************/
void termFUNCTION1(){
	
}