/*******************************************************************************
 *
 *  Purpose: Header for FUNCTION2 component
 *
 *
 *  Date:
 *  Author: 
 *  
 *******************************************************************************/
 /******************************************************************************
 * Includes
 *******************************************************************************/

 /******************************************************************************
 * DEFINES
 *******************************************************************************/
// Define guards for this header 
#ifndef FUNCTION2_H
#define FUNCTION2_H

// Some parameter related to FUNCTION2
#define FUNCTION2_PARAMETER 0

// Define guards to make sure .c functions/objects play nicely with .cpp functions/objects
#ifdef __cplusplus
extern "C" {
#endif

 /******************************************************************************
 * Function Prototypes
 *******************************************************************************/
void iniFUNCTION2();
float FUNCTION2(int input1, int *input2, int *output1, int *output2);
void termFUNCTION2();

#ifdef __cplusplus
}
#endif //__cplusplus
#endif //FUNCTION2_C