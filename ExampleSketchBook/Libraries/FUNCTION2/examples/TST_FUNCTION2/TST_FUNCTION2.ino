/*******************************************************************************
   TST_XYZ

    Purpose: code that implments a test of FUCNTION2 component


    Date:
    Author:

    test procedure document to follow etc.
 *******************************************************************************/
/******************************************************************************
  Includes
*******************************************************************************/
#include<FUNCTION2.h>
/******************************************************************************
  DEFINES
*******************************************************************************/

/******************************************************************************
  Declaration of Global Variables
*******************************************************************************/


/******************************************************************************
   Setup function

    purpose :

    inputs: N/A
    outputs: N/A
    effect: initalizes pins and drivers to be used by main loop
 ******************************************************************************/
void setup() {
  // put your setup code here, to run once:
  iniFUNCTION2();
  // Enable Serial port (so we can print results to understand whats going on)
  Serial.begin(115200);
  Serial.println("Initialized TST_FUNCTION2 ");

}

/******************************************************************************
   loop function

    purpose : implements the main procedure of .....

    inputs: N/A
    outputs: N/A
    effect: executes the functional flow of ....
 ******************************************************************************/
void loop() {
  // put your main code here, to run repeatedly:

  int i;
  int input1 = 3;
  int input2[3] = {4, 5, 6};
  int output1;
  int output2[3];
  float returnvalue;

  returnvalue = FUNCTION2(input1, input2, &output1, output2);

  Serial.print("input1 ");
  Serial.print(input1);
  Serial.print(" input2 =[" );
  for (i = 0; i < 3 ; i++) {
    Serial.print(input2[i]);
    Serial.print(" " );
  }

  Serial.print("] output1 ");
  Serial.print(output1);

  Serial.print(" output2 =[" );
  for (i = 0; i < 3 ; i++) {
    Serial.print(output2[i]);
    Serial.print(" " );
  }
  Serial.print("] returnvalue ");
  Serial.println(returnvalue);

  delay(2000);

}
