/*******************************************************************************
 * FUNCTION2
 *
 *  Purpose: code that implements FUNCTION2 component
 *
 *
 *  Date:
 *  Author: 
 *  
 *******************************************************************************/
 /******************************************************************************
 * Includes
 *******************************************************************************/
#include <FUNCTION2.h>

 /******************************************************************************
  * iniFUNCTION2
  *
  *  purpose : initializes pins and memory used by FUNCTION2
  *
  *  inputs: N/A
  *  outputs: N/A
  *  effect: 
  ******************************************************************************/
void iniFUNCTION2() {
  // put your setup code here, to run once:

}

 /******************************************************************************
  * FUNCTION2
  *
  *  purpose : implements the main procedure of dummy function with inputs outputs and return statements
  *
  *  inputs: 	input1 a single integer 
  *     		input2 a vector of 3 integers (size not enforced in c)
  *  outputs: 	output1 a single integer 
  *				output2	a vector of 3 integers
  *  return: 	an integer
  *  effect: main routine of FUNCTION2. 
  ******************************************************************************/
float FUNCTION2(int input1, int *input2, int *output1, int *output2) {
	float returnvalue;
    float temp;
	// multiply the vector by the scalar  
	output2[0] = input1*input2[0];
	output2[1] = input1*input2[1];
	output2[2] = input1*input2[2];

	// compute returnvalue = (ouput2{i}^2)
	 *output1 = output2[0]*output2[0] + output2[1]*output2[1] + output2[2]*output2[2];

	// set returnvalue the sqrt of output1
	temp = (float)(*output1);
	returnvalue = sqrt(temp);

	// end the function by returning returnvalue
	return returnvalue;
}

 /******************************************************************************
  * iniFUNCTION2
  *
  *  purpose : terminates anything initialized for FUNCTION2
  *
  *  inputs: N/A
  *  outputs: N/A
  *  effect: 
  ******************************************************************************/
void termFUNCTION2(){
	
}