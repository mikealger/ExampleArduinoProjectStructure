This this library is yet another example/template for spliting up your project into smaller testable chunks. 

This Function (a clone of FUNCTION2) demonstrates the minimum required to to prepare a library for the latest specification of the Arduino library. 

In additon to the library.properties file you may want to add a keywords.txt to have arduinos code highlighting tool highlight different code elements. Finally the existance of "".development" file is to disable the write protect features of the IDE 

The details of the library specfication can be found at: 
https://github.com/arduino/Arduino/wiki/Arduino-IDE-1.5:-Library-specification

