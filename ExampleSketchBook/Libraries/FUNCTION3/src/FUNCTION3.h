/*******************************************************************************
 *
 *  Purpose: Header for FUNCTION3 component
 *
 *
 *  Date:
 *  Author: 
 *  
 *******************************************************************************/
 /******************************************************************************
 * Includes
 *******************************************************************************/

 /******************************************************************************
 * DEFINES
 *******************************************************************************/
// Define guards for this header 
#ifndef FUNCTION3_H
#define FUNCTION3_H

// Some parameter related to FUNCTION3
#define FUNCTION3_PARAMETER 0

// Define guards to make sure .c functions/objects play nicely with .cpp functions/objects
#ifdef __cplusplus
extern "C" {
#endif

 /******************************************************************************
 * Function Prototypes
 *******************************************************************************/
void iniFUNCTION3();
float FUNCTION3(int input1, int *input2, int *output1, int *output2);
void termFUNCTION3();

#ifdef __cplusplus
}
#endif //__cplusplus
#endif //FUNCTION3_C