/*******************************************************************************
   TST_XYZ

    Purpose: code that implements a test of XYZ component


    Date:
    Author:

    test procedure document to follow etc.
 *******************************************************************************/
/******************************************************************************
  Includes
*******************************************************************************/
#include <FUNCTION1.h>
#include <FUNCTION2.h>
/******************************************************************************
  DEFINES
*******************************************************************************/

/******************************************************************************
  Declaration of Global Variables
*******************************************************************************/


/******************************************************************************
   Setup function

    purpose :

    inputs: N/A
    outputs: N/A
    effect: initializes pins and drivers to be used by main loop
 ******************************************************************************/
void setup() {
  // put your setup code here, to run once:
  iniFUNCTION1();
  iniFUNCTION2();
  // Enable Serial port (so we can print results to understand whats going on)
  Serial.begin(115200);
  Serial.println("Initialized SAT_FullyIntegratedSystem Test");
}

/******************************************************************************
   loop function

    purpose : implements the main procedure of .....

    inputs: N/A
    outputs: N/A
    effect: executes the functional flow of ....
 ******************************************************************************/
void loop() {
  int i;
  int input1 = 3;
  int input2[3] = {4, 5, 6};
  int output1;
  int output2[3];
  float returnvalue;

  //Call Function1
  delay(FUNCTION1_PARAMETER); //wait the length of time from the parameter defined in FUNCTION1.h
  FUNCTION1();

  // Wait 10 ms before running function2
  delay(10);
  returnvalue = FUNCTION2(input1, input2, &output1, output2);


  // Display inputs and outupts from FUNCTION2 as part of this test case
  Serial.print("input1 ");
  Serial.print(input1);
  Serial.print(" input2 =[" );
  for (i = 0; i < 3 ; i++) {
    Serial.print(input2[i]);
    Serial.print(" " );
  }

  Serial.print("] output1 ");
  Serial.print(output1);

  Serial.print(" output2 =[" );
  for (i = 0; i < 3 ; i++) {
    Serial.print(output2[i]);
    Serial.print(" " );
  }
  Serial.print("] returnvalue ");
  Serial.println(returnvalue);

  delay(2000);
}
