To configure Arduino IDE to compile code within this folder; 
----
 * Set Arduino IDE sketchbook folder to this root directory 
   * i.e. >>FILE>>Preferences, on the Settings tab modify the "Sketchbook location" parameter to this directory.
   * this forces the compiler to first use libraries found in the Libraries folder of this sketchbook folder, if in future you need any more libraries unpack them in the Libraries folder, so they can be versioned with this projects source code. 

Summary of tests found here
-----
The Fully Integrated System (i.e. main/final software) can be found in the Systems acceptance test folder 
"SAT_FullyIntegratedSystem\SAT_FullyIntegratedSystem.ino"

Integration tests integrating various subsystems can be found in similarly named folders at this level. 

Unit tests are found within their subcomponent example folders:
for instance, the unit test for FUNCTION1 can be found under "Libraries/FUNCTION1/examples/TST_FUNCTION1.ino"

